# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Create donors
Donor.create(name: 'Taras Boichenko', email: 'taras@gmail.com')
Donor.create(name: 'Kevang Desai', email: 'kevang@gmail.com')
Donor.create(name: 'Bashar Shamma', email: 'bashar@gmail.com')
Donor.create(name: 'Jessica De Luna', email: 'jessica@gmail.com')

#Cteate Donations
Donation.create(donor_id: 1, amount: 150.57, date: Date.today)
Donation.create(donor_id: 1, amount: 100.57, date: Date.today)
Donation.create(donor_id: 2, amount: 34.22, date: Date.today)
Donation.create(donor_id: 2, amount: 12.33, date: Date.today)
Donation.create(donor_id: 3, amount: 12.67, date: Date.today)
Donation.create(donor_id: 3, amount: 200.11, date: Date.today)
Donation.create(donor_id: 4, amount: 654.21, date: Date.today)
Donation.create(donor_id: 4, amount: 87.15, date: Date.today)


